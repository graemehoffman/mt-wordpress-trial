<?php
/*
 * PODCAST Archive Template
 *
 * Displays the single version of the podacst
 *
 * 
*/

?>


<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

	<header class="article-header">

		<h3 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		
		<div>
		<?php the_post_thumbnail( 'medium' ); ?>
		</div>
		
		<p class="byline vcard"><?php
			printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> by <span class="author">%3$s</span>', 'trialbones' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'trialbones' ) ), get_author_posts_url( get_the_author_meta( 'ID' ) ));
		?></p>

	</header>

	<section class="entry-content cf">

		<?php the_excerpt(); ?>
    
    <?php the_podcast_play_button(); ?>
    
	</section>

	<footer class="article-footer">

	</footer>

</article>