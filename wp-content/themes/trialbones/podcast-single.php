<?php
/*
 * PODCAST Single Template
 *
 * Displays the single version of the podacst
 *
 * 
*/

?>



<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

	<header class="article-header">
    
		<h1 class="single-title custom-post-type-title"><?php the_title(); ?></h1>
		
		<div class="img-responsive">
		<?php the_post_thumbnail( 'feature-thumb' ); ?>
		</div>
		
		<p class="byline vcard"><?php
			printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')), get_the_author_link( get_the_author_meta( 'ID' ) ), get_the_term_list( $post->ID, 'custom_cat', ' ', ', ', '' ) );
		?></p>

	</header>

	<section class="entry-content cf">
		<?php
			// the content (pretty self explanatory huh)
			the_content();
		?>
		
		<?php the_podcast_play_button(); ?>
		
		
		
		
	</section> <!-- end article section -->

	<footer class="article-footer">
		<p class="tags"><?php echo get_the_term_list( get_the_ID(), 'custom_tag', '<span class="tags-title">' . __( 'Custom Tags:', 'bonestheme' ) . '</span> ', ', ' ) ?></p>

	</footer>

	<?php comments_template(); ?>

</article>