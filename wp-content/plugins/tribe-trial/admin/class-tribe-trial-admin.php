<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://graemehoffman.com
 * @since      1.0.0
 *
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/admin
 * @author     Graeme Hoffman <graeme@graemehoffman.com>
 */
class Tribe_Trial_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

  /**
	 * Creates post types
	 *
	 * @since    1.0.0
	 */
	public function _create_posttypes() {
    
    $labels = array(
    		'name'                => _x( 'Podcasts', 'Post Type General Name', 'tribe-trial' ),
    		'singular_name'       => _x( 'Podcast', 'Post Type Singular Name', 'tribe-trial' ),
    		'menu_name'           => __( 'Podcasts', 'tribe-trial' ),
    		'name_admin_bar'      => __( 'Podcast', 'tribe-trial' ),
    		'parent_item_colon'   => __( 'Parent Podcast:', 'tribe-trial' ),
    		'all_items'           => __( 'All Podcast', 'tribe-trial' ),
    		'add_new_item'        => __( 'Add New Podcast', 'tribe-trial' ),
    		'add_new'             => __( 'Add New', 'tribe-trial' ),
    		'new_item'            => __( 'New Podcast', 'tribe-trial' ),
    		'edit_item'           => __( 'Edit Podcast', 'tribe-trial' ),
    		'update_item'         => __( 'Update Podcast', 'tribe-trial' ),
    		'view_item'           => __( 'View Podcast', 'tribe-trial' ),
    		'search_items'        => __( 'Search Podcast', 'tribe-trial' ),
    		'not_found'           => __( 'Not found', 'tribe-trial' ),
    		'not_found_in_trash'  => __( 'Not found in Trash', 'tribe-trial' ),
    	);
    	$args = array(
    		'label'               => __( 'podcast', 'tribe-trial' ),
    		'description'         => __( 'Custom Type for Tribe Trial', 'tribe-trial' ),
    		'labels'              => $labels,
    		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
    		//'taxonomies'          => array( 'category', 'post_tag' ),
    		'hierarchical'        => false,
    		'public'              => true,
    		'show_ui'             => true,
    		'show_in_menu'        => true,
    		'menu_position'       => 5,
    		'show_in_admin_bar'   => true,
    		'show_in_nav_menus'   => true,
    		'can_export'          => true,
    		'has_archive'         => true,
    		'exclude_from_search' => false,
    		'publicly_queryable'  => true,
    		'capability_type'     => 'post',
    	);
    	register_post_type( 'podcast', $args );
    	
    	
    	
    
	}
	
	
  /**
	 * Adds custom taxes 
	 *
	 * @since    1.0.0
	 */
	public function _register_taxonomies() {
  
    // now let's add custom categories (these act like categories)
  	register_taxonomy( 'custom_cat', 
  		array('podcast'), 
  		array('hierarchical' => true,     /* if this is true, it acts like categories */
  			'labels' => array(
  				'name' => __( 'Custom Categories', 'trialbones' ), /* name of the custom taxonomy */
  				'singular_name' => __( 'Custom Category', 'trialbones' ), /* single taxonomy name */
  				'search_items' =>  __( 'Search Custom Categories', 'trialbones' ), /* search title for taxomony */
  				'all_items' => __( 'All Custom Categories', 'trialbones' ), /* all title for taxonomies */
  				'parent_item' => __( 'Parent Custom Category', 'trialbones' ), /* parent title for taxonomy */
  				'parent_item_colon' => __( 'Parent Custom Category:', 'trialbones' ), /* parent taxonomy title */
  				'edit_item' => __( 'Edit Custom Category', 'trialbones' ), /* edit custom taxonomy title */
  				'update_item' => __( 'Update Custom Category', 'trialbones' ), /* update title for taxonomy */
  				'add_new_item' => __( 'Add New Custom Category', 'trialbones' ), /* add new title for taxonomy */
  				'new_item_name' => __( 'New Custom Category Name', 'trialbones' ) /* name title for taxonomy */
  			),
  			'show_admin_column' => true, 
  			'show_ui' => true,
  			'query_var' => true,
  			'rewrite' => array( 'slug' => 'custom-slug' ),
  		)
  	);
	
  	// now let's add custom tags (these act like categories)
  	register_taxonomy( 'custom_tag', 
  		array('podcast'), 
  		array('hierarchical' => false,    /* if this is false, it acts like tags */
  			'labels' => array(
  				'name' => __( 'Custom Tags', 'tribe-trial' ), /* name of the custom taxonomy */
  				'singular_name' => __( 'Custom Tag', 'tribe-trial' ), /* single taxonomy name */
  				'search_items' =>  __( 'Search Custom Tags', 'trialbones' ), /* search title for taxomony */
  				'all_items' => __( 'All Custom Tags', 'trialbones' ), /* all title for taxonomies */
  				'parent_item' => __( 'Parent Custom Tag', 'trialbones' ), /* parent title for taxonomy */
  				'parent_item_colon' => __( 'Parent Custom Tag:', 'trialbones' ), /* parent taxonomy title */
  				'edit_item' => __( 'Edit Custom Tag', 'trialbones' ), /* edit custom taxonomy title */
  				'update_item' => __( 'Update Custom Tag', 'trialbones' ), /* update title for taxonomy */
  				'add_new_item' => __( 'Add New Custom Tag', 'trialbones' ), /* add new title for taxonomy */
  				'new_item_name' => __( 'New Custom Tag Name', 'trialbones' ) /* name title for taxonomy */
  			),
  			'show_admin_column' => true,
  			'show_ui' => true,
  			'query_var' => true,
  		)
  	);
  
   }

  
  
  /**
	 * Init hook
	 *
	 * @since    1.0.0
	 */
	public function init() {
	  
	  $this->_create_posttypes();
	  $this->_register_taxonomies();
	  
	  
  }
  
  
  /**
	 * Fields
	 *
	 * @since    1.0.0
	 */
	public function cmb2_init() {
	  
	  // Start with an underscore to hide fields from custom fields list
    	$prefix = '_tribetrial_demo_';
	  
	  /**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	 
    	$cmb_demo = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => __( 'Custom Fields', 'tribe-trial' ),
    		'object_types'  => array( 'podcast', ), // Post type
    		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
    		// 'context'    => 'normal',
    		// 'priority'   => 'high',
    		// 'show_names' => true, // Show field names on the left
    		// 'cmb_styles' => false, // false to disable the CMB stylesheet
    		// 'closed'     => true, // true to keep the metabox closed by default
    	) );
    	$cmb_demo->add_field( array(
    		'name'       => __( 'Podcast URL', 'tribe-trial' ),
    		//'desc'       => __( 'field description (optional)', 'cmb2' ),
    		'id'         => $prefix . 'url',
    		'type'       => 'text',
    		//'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
    		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
    		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
    		// 'on_front'        => false, // Optionally designate a field to wp-admin only
    		// 'repeatable'      => true,
    	) );
    	
    	
    	
	  
  }
  
  
  
  
  
  
	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tribe_Trial_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tribe_Trial_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tribe-trial-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tribe_Trial_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tribe_Trial_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tribe-trial-admin.js', array( 'jquery' ), $this->version, false );

	}

}
