<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://graemehoffman.com
 * @since             1.0.0
 * @package           Tribe_Trial
 *
 * @wordpress-plugin
 * Plugin Name:       Tribe Triabe
 * Plugin URI:        http://graemehoffman.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Graeme Hoffman
 * Author URI:        http://graemehoffman.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tribe-trial
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Get the CMB bootstrap! 
 */
if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tribe-trial-activator.php
 */
function activate_tribe_trial() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tribe-trial-activator.php';
	Tribe_Trial_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tribe-trial-deactivator.php
 */
function deactivate_tribe_trial() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tribe-trial-deactivator.php';
	Tribe_Trial_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tribe_trial' );
register_deactivation_hook( __FILE__, 'deactivate_tribe_trial' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tribe-trial.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tribe_trial() {

	$plugin = new Tribe_Trial();
	$plugin->run();

}
run_tribe_trial();
