<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://graemehoffman.com
 * @since      1.0.0
 *
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
