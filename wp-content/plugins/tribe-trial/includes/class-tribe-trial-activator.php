<?php

/**
 * Fired during plugin activation
 *
 * @link       http://graemehoffman.com
 * @since      1.0.0
 *
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/includes
 * @author     Graeme Hoffman <graeme@graemehoffman.com>
 */
class Tribe_Trial_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
