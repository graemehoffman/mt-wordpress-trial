<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://graemehoffman.com
 * @since      1.0.0
 *
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tribe_Trial
 * @subpackage Tribe_Trial/includes
 * @author     Graeme Hoffman <graeme@graemehoffman.com>
 */
class Tribe_Trial_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
